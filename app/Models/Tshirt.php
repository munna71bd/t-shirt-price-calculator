<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Tshirt extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'created_by', 'updated_by'];

    public function rawMaterials()
    {
        return $this->hasMany(TshirtRawMaterial::class);
    }


    public function totalPrice()
    {
        $total_price = 0;

        foreach ($this->rawMaterials as $trm) {
            $total_price += $trm->rawMaterial->price * $trm->qty;
        }
        return $total_price;
    }

    public function givenQty($raw_material_id)
    {
        $qty = 0;
        $data = TshirtRawMaterial::select(['qty'])
            ->where(['tshirt_id' => $this->id, 'raw_material_id' => $raw_material_id])
            ->first();

        if ($data) {
            $qty = $data->qty;
        }
        return $qty;

    }
}
