<?php

namespace App\Http\Controllers;

use App\Models\RawMaterial;
use App\Models\Tshirt;
use App\Models\TshirtRawMaterial;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TshirtController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Tshirt::orderBy('created_at', 'DESC')->get(['id', 'name']);
        return view('t-shirt.browse', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $raw_materials = RawMaterial::get();
        return view('t-shirt.add-edit', compact('raw_materials'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            $rules = [
                'name' => 'required|unique:tshirts',
                'raw_materials_qty.*' => 'required|numeric',
            ];

            $data = $request->except(['_token']);


            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {

                return response()->json([
                    'status' => 'error',
                    'message' => $validator->messages()
                ]);
            }

            $data['created_by'] = auth()->user()->id;
            $tshirt = Tshirt::create($data);

            if ($tshirt) {
                $inputrawMaterialData = $request->except(['_token', 'name']);
                $rawMaterialData = [];

                foreach ($inputrawMaterialData['raw_materials_qty'] as $k => $qty) {

                    $is_rm_in_db = RawMaterial::where('id', $k)->count();

                    if ($is_rm_in_db > 0) {
                        $rawMaterialData [] = [
                            'tshirt_id' => $tshirt->id,
                            'raw_material_id' => $k,
                            'qty' => $qty,
                            'created_by' => auth()->user()->id
                        ];
                    }
                }

                if (TshirtRawMaterial::insert($rawMaterialData)) {

                    return response()->json([
                        'status' => 'success',
                        'message' => 'New T-shirt Crated Successfully.'
                    ]);
                }

            }

            return response()->json([
                'status' => 'success',
                'message' => 'Something went wrong!'
            ]);


        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Tshirt $tshirtController
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = Tshirt::findorfail($id);
        $raw_materials = RawMaterial::get();
        return view('t-shirt.view', compact('model','raw_materials'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Tshirt $model
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Tshirt::findorfail($id);
        $raw_materials = RawMaterial::get();
        return view('t-shirt.add-edit', compact('raw_materials', 'model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Tshirt $tshirtController
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {

            $rules = [
                'name' => 'required',
                'raw_materials_qty.*' => 'required|numeric',
            ];

            $data = $request->except(['_token']);


            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {

                return response()->json([
                    'status' => 'error',
                    'message' => $validator->messages()
                ]);
            }


            $tshirt = Tshirt::findorfail($id);

            if ($tshirt) {
                $data['updated_by'] = auth()->user()->id;
                $tshirt->update($data);


                $inputrawMaterialData = $request->except(['_token', 'name']);
                $rawMaterialData = [];

                foreach ($inputrawMaterialData['raw_materials_qty'] as $k => $qty) {

                    $is_rm_in_db = RawMaterial::where('id', $k)->count();

                    if ($is_rm_in_db > 0) {
                        $rawMaterialData [] = [
                            'tshirt_id' => $tshirt->id,
                            'raw_material_id' => $k,
                            'qty' => $qty,
                            'updated_by' => auth()->user()->id
                        ];
                    }
                }

                if (TshirtRawMaterial::where('tshirt_id', $tshirt->id)->delete() && TshirtRawMaterial::insert($rawMaterialData)) {

                    return response()->json([
                        'status' => 'success',
                        'message' => 'T-shirt Updated Successfully.'
                    ]);
                }

            }

            return response()->json([
                'status' => 'success',
                'message' => 'Something went wrong!'
            ]);


        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Tshirt $tshirtController
     * @return \Illuminate\Http\Response
     */
    public function destroy(TshirtController $tshirtController)
    {
        //
    }

    public function checkNameAvailability(Request $request)
    {
        try {
            $is_available = Tshirt::where('name', $request->name)->where('id', '<>', $request->model_id)->count();

            if ($is_available > 0) {
                return response()->json(false);
            }
            return response()->json(true);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ]);
        }

    }

}
