<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
Route::get('/register', function () {
    abort(404);
});
Route::post('/register', function () {
    abort(401);
});

Route::get('/', 'App\Http\Controllers\TshirtController@create');
Route::resource('/t-shirts', 'App\Http\Controllers\TshirtController');
Route::post('/t-shirts/check/name/availability', 'App\Http\Controllers\TshirtController@checkNameAvailability')->name('t-shirts.check.name.availability');


