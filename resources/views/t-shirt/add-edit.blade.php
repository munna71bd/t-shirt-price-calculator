@extends('layouts.admin.app')
@section('css')
    <style>
        .table td, .table th {
            padding: 8px;
        }

        .form-control {
            height: 40px;
        }
    </style>
@endsection
@section('content')

    <section class="pcoded-main-container">


        <div class="modal-dialog modal-xl" role="document">
            <form
                action="@if(isset($model) && $model) {{route('t-shirts.update',$model)}} @else {{route('t-shirts.store')}} @endif"
                id="add-edit-form" method="POST">
                @csrf
                @if(isset($model) && $model)
                    @method('patch')
                @endif


                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLiveLabel">
                            @if(isset($model) && $model) Edit @else Create New @endif T-shirt
                        </h5>

                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for=""><span class="text-danger">*</span>T-shirt Name :</label>
                                    <input type="text" class="form-control mandatory" name="name"
                                           value="{{ isset($model) ? $model->name : ''}}"
                                           placeholder="Enter T-shirt name">
                                    <span class="error_title error"></span>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <label>Total Price:</label>
                                <span class="form-control text-success"
                                      id="total_price">{{ isset($model) ? $model->totalPrice() : ''}}</span>

                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <h5>Raw Materials</h5>
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Unit</th>
                                        <th>Price</th>
                                        <th width="15%">Qty</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($raw_materials as $k => $data)
                                        <tr>
                                            <td>
                                                <img height="30px;" src="{{url('/')}}/{{$data->icon}}"/>
                                                {{$data->name}}
                                            </td>
                                            <td>{{$data->unit}}</td>
                                            <td>{{$data->price}}</td>
                                            <td>
                                                <input class="form-control  digit" type="tel"
                                                       name="raw_materials_qty[{{$data->id}}]"
                                                       data-price="{{$data->price}}"
                                                       value="{{isset($model) && $model ? $model->givenQty($data->id) : 0}}">
                                            </td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                    <tfoot>
                                    <tr>
                                    </tr>
                                    </tfoot>
                                </table>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button id="final_submit" class="btn btn-sm btn-primary"><i class="feather icon-save"></i>
                            @if(isset($model) && $model) Update @else Save @endif
                        </button>
                    </div>
                </div>
            </form>
        </div>


    </section>



@endsection

@section('javascript')
    <script src="{{asset('backend_resources/js/jquery_validation_combine.js')}}"></script>
    <script>
        var totalPrice = 0;
        var is_edit = '{{isset($model) && $model ? 1: 0}}';

        function loaderToggle(t) {
            if (t == 1) {
                $('.loader-bg').fadeIn('slow')
            } else {
                $('.loader-bg').fadeOut('fast')
            }
        }


        $("#add-edit-form").validate({
            rules: {
                name: {
                    required: true,
                    remote: {
                        url: "{{route('t-shirts.check.name.availability')}}",
                        type: 'POST',
                        data: {model_id: '{{ isset($model) && $model ? $model->id: 0 }}'},
                        delay: 500,
                    }
                },


            },
            messages: {
                name: {
                    remote: "This name has already been taken!"
                }
            },

            submitHandler: function (form, e) {
                loaderToggle(1)
                save()
            }
        });


        function save() {

            $.ajax({
                url: $('#add-edit-form').attr('action'),
                type: $('#add-edit-form').attr('method'),
                cache: false,
                data: $('#add-edit-form').serialize(),
                success: function (response) {
                    loaderToggle(0)
                    $('.error_title').empty();
                    $('.error_price').empty();
                    if (response.status == 'success') {
                        toastr.success(response.message);

                        if (is_edit == 0) {
                            $('input').val('')
                            setTotalPrice()
                        }

                    } else {
                        toastr.error(response.message);
                    }
                },
                error: function (response) {
                    loaderToggle(0)
                    console.log(response)
                }

            });

        }

        $('.mandatory').each(function () {
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "This field is required.",
                },
            });
        });

        $('.digit').each(function () {
            $(this).rules("add", {
                digits: true,
                required: true,
                minlength: 1

            });
        });

        $('.digit').on('input', function () {
            setTotalPrice()
        });


        function setTotalPrice() {
            totalPrice = 0;
            $('.digit').each(function () {
                totalPrice += parseInt($(this).data('price')) * parseInt(this.value)
            });
            $('#total_price').html(totalPrice)
        }

        $('[type=tel]').on('change', function (e) {
            $(e.target).val($(e.target).val().replace(/[^\d]/g, ''))
        })
        $('[type=tel]').on('keypress', function (e) {
            keys = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
            return keys.indexOf(event.key) > -1
        })


    </script>

@endsection
