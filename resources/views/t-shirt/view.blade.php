@extends('layouts.admin.app')
@section('css')
    <style>
        .table td, .table th {
            padding: 8px;
        }

        .form-control {
            height: 40px;
        }
    </style>
@endsection
@section('content')

    <section class="pcoded-main-container">


        <div class="modal-dialog modal-xl" role="document">


            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLiveLabel">
                       View T-shirt
                    </h5>

                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for=""><span class="text-danger"></span>T-shirt Name :</label>
                                <input type="text" class="form-control mandatory" name="name"
                                       value="{{ isset($model) ? $model->name : ''}}"
                                       placeholder="Enter T-shirt name">
                                <span class="error_title error"></span>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <label>Total Price:</label>
                            <span class="form-control text-success"
                                  id="total_price">{{ isset($model) ? $model->totalPrice() : ''}}</span>

                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <h5>Raw Materials</h5>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Unit</th>
                                    <th>Price</th>
                                    <th width="15%">Qty</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($raw_materials as $k => $data)
                                    <tr>
                                        <td>
                                            <img height="30px;" src="{{url('/')}}/{{$data->icon}}"/>
                                            {{$data->name}}
                                        </td>
                                        <td>{{$data->unit}}</td>
                                        <td>{{$data->price}}</td>
                                        <td>
                                            <input class="form-control  digit" type="tel"
                                                   name="raw_materials_qty[{{$data->id}}]"
                                                   data-price="{{$data->price}}"
                                                   value="{{isset($model) && $model ? $model->givenQty($data->id) : 0}}">
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                                <tfoot>
                                <tr>
                                </tr>
                                </tfoot>
                            </table>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">

                </div>
            </div>
        </div>


    </section>


@endsection

@section('javascript')
    <script>
        $('input').attr('readonly',true)
    </script>

@endsection
