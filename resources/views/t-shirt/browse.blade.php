@extends('layouts.admin.app')

@section('title')

@endsection

@section('css')
    <style>
        .table > :not(:first-child) {
            border-top: 0;
        }
    </style>
@endsection

@section('content')
    <section class="pcoded-main-container">
        <div class="pcoded-content">
            <!-- [ breadcrumb ] start -->
            <div class="container">

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="portlet-title">
                                <div class="tools pull-right">
                                    <div class="m-3">
                                        <b>T-shirt List</b>
                                    </div>
                                    <button onclick="newTshirt()" type="button" class="btn btn-sm btn-success"
                                            data-toggle="modal" data-target="#addPackage"><i class="fa fa-plus"></i>
                                    </button>
                                </div>
                            </div>
                            <hr class="m-0">
                            <div class="card-body">
                                <table id="dataTable" class="table table-striped table-sm table-hover text-center">
                                    <thead class="text-center">
                                    <tr class="text-center">
                                        <th>Sl</th>
                                        <th>Name</th>
                                        <th>Price</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>

                                    <tbody id="packagesBody">

                                    @foreach($data as $k => $d)
                                        <tr>
                                            <td>{{$k+1}}</td>
                                            <td>{{$d->name}}</td>
                                            <td>{{$d->totalPrice()}}</td>
                                            <td>
                                                <a class="btn btn-secondary"
                                                   href="{{route('t-shirts.edit',$d)}}">Edit</a>
                                                <a class="btn btn-info" href="{{route('t-shirts.show',$d)}}">View</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>



@endsection

@section('javascript')

    <script>

        function newTshirt() {
            location = "{{route('t-shirts.create')}}"
        }
    </script>

@endsection
