<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'CMS') }}</title>

    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="icon" href="{{asset('assets/images/favicon.ico')}}" type="image/x-icon">
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <link rel="stylesheet" href="http://cdn.bootcss.com/toastr.js/latest/css/toastr.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('backend_resources/css/datatable/datatables.min.css') }}">
    <style>
        div.dataTables_wrapper div.dataTables_filter input {
            width: 175px;
        }

        .pull-right {
            justify-content: space-between;
            display: flex;
            margin: 10px 10px 10px 0px;
        }

        .pull-right button {
            height: 40px;
            width: 40px;
            border-radius: 50%;
        }

        .fixed-button {
            display: none;
        }

        .error {
            color: red !important;
        }

        #itemBody a {
            color: white;
        }

        .btn-success {
            background: #1abc9c !important;
            border: none;
            padding: 8px;
        }

        .table.dataTable {
            width: 100% !important;
        }
    </style>
    @yield('css')
</head>
<body>
<div id="app">
    <div class="loader-bg">
        <div class="loader-track">
            <div class="loader-fill"></div>
        </div>
    </div>
    <!-- Add sidebar and nav bar -->
    @if(auth()->user())
        @include('layouts.admin.sidebar')
        @include('layouts.admin.nav-bar')
    @endif
    @yield('content')
</div>
</body>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="{{asset('assets/js/vendor-all.min.js')}}"></script>
<script src="{{asset('assets/js/plugins/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/pcoded.min.js')}}"></script>
<script src="{{asset('assets/js/plugins/apexcharts.min.js')}}"></script>
<!-- custom-chart js -->
{{--<script src="{{asset('assets/js/pages/dashboard-main.js')}}"></script>--}}
<script src="{{ asset('backend_resources/js/datatable/datatables.min.js') }}"></script>
<script src="http://cdn.bootcss.com/toastr.js/latest/js/toastr.min.js"></script>
@yield('javascript')
<script>
    $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
    // for removing backdrop of a modal
    $('.modal').attr({
        "data-backdrop": "static",
        "data-keyboard": false
    });
</script>

</html>
