<nav class="pcoded-navbar  ">
    <div class="navbar-wrapper  ">
        <div class="navbar-content scroll-div " >

            <div class="">
                <div class="main-menu-header">
                    <img class="img-radius" src="{{url('/')}}/{{Auth::user()->avatar}}" alt="User-Profile-Image">
                    <div class="user-details">
                        <span>{{ Auth::user()->name }}</span>
                       </div>
                </div>
            </div>

            <ul class="nav pcoded-inner-navbar ">


                <li class="nav-item">
                    <a href="{{route('t-shirts.index')}}" class="nav-link "><span class="pcoded-micon"><i class="fas fa-tshirt"></i></span><span class="pcoded-mtext">T-shirts</span></a>
                </li>
                <li class="nav-item">
                    <a href="{{route('t-shirts.create')}}" class="nav-link "><span class="pcoded-micon"><i class="fas fa-plus-square"></i></span><span class="pcoded-mtext">New T-shirts</span></a>
                </li>





            </ul>

        </div>
    </div>
</nav>
