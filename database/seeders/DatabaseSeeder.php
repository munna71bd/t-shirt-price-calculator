<?php

namespace Database\Seeders;

use App\Models\RawMaterial;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //\App\Models\User::factory(1)->create();
        $raw_materials_data = [
            [
                'name' => 'Thread',
                'unit' => 'kg',
                'price' => 500.00
            ],
            [
                'name' => 'Fabric',
                'unit' => 'kg',
                'price' => 2000.00
            ],
            [
                'name' => 'Button',
                'unit' => 'piece',
                'price' => 15.00
            ],
            [
                'name' => 'Care Label',
                'unit' => 'piece',
                'price' => 5.00
            ],
            [
                'name' => 'Label',
                'unit' => 'piece',
                'price' => 2.00
            ],
        ];
        RawMaterial::insert($raw_materials_data);
    }
}
