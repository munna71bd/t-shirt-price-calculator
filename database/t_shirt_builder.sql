/*
 Navicat Premium Data Transfer

 Source Server         : Xaamp
 Source Server Type    : MySQL
 Source Server Version : 100420
 Source Host           : localhost:3306
 Source Schema         : t_shirt_builder

 Target Server Type    : MySQL
 Target Server Version : 100420
 File Encoding         : 65001

 Date: 25/06/2022 04:42:19
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for failed_jobs
-- ----------------------------
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `failed_jobs_uuid_unique`(`uuid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of failed_jobs
-- ----------------------------

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2019_08_19_000000_create_failed_jobs_table', 1);
INSERT INTO `migrations` VALUES (4, '2019_12_14_000001_create_personal_access_tokens_table', 1);
INSERT INTO `migrations` VALUES (5, '2022_06_24_100001_create_raw_materials_table', 2);
INSERT INTO `migrations` VALUES (6, '2022_06_24_100002_create_tshirts_table', 2);
INSERT INTO `migrations` VALUES (7, '2022_06_24_100003_create_tshirt_raw_materials_table', 2);

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for personal_access_tokens
-- ----------------------------
DROP TABLE IF EXISTS `personal_access_tokens`;
CREATE TABLE `personal_access_tokens`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `personal_access_tokens_token_unique`(`token`) USING BTREE,
  INDEX `personal_access_tokens_tokenable_type_tokenable_id_index`(`tokenable_type`, `tokenable_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of personal_access_tokens
-- ----------------------------

-- ----------------------------
-- Table structure for raw_materials
-- ----------------------------
DROP TABLE IF EXISTS `raw_materials`;
CREATE TABLE `raw_materials`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `unit` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'kg',
  `price` decimal(8, 2) NOT NULL DEFAULT 0.00,
  `created_by` int UNSIGNED NULL DEFAULT NULL,
  `updated_by` int UNSIGNED NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `raw_materials_name_unique`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of raw_materials
-- ----------------------------
INSERT INTO `raw_materials` VALUES (1, 'Thread', 'kg', 500.00, NULL, NULL, NULL, NULL, 'storage/raw_materials/t.png');
INSERT INTO `raw_materials` VALUES (2, 'Fabric', 'kg', 2000.00, NULL, NULL, NULL, NULL, 'storage/raw_materials/f.png');
INSERT INTO `raw_materials` VALUES (3, 'Button', 'piece', 15.00, NULL, NULL, NULL, NULL, 'storage/raw_materials/b.png');
INSERT INTO `raw_materials` VALUES (4, 'Care Label', 'piece', 5.00, NULL, NULL, NULL, NULL, 'storage/raw_materials/c.png');
INSERT INTO `raw_materials` VALUES (5, 'Label', 'piece', 2.00, NULL, NULL, NULL, NULL, 'storage/raw_materials/l.png');

-- ----------------------------
-- Table structure for tshirt_raw_materials
-- ----------------------------
DROP TABLE IF EXISTS `tshirt_raw_materials`;
CREATE TABLE `tshirt_raw_materials`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `tshirt_id` int UNSIGNED NOT NULL,
  `raw_material_id` int UNSIGNED NOT NULL,
  `qty` int UNSIGNED NOT NULL,
  `created_by` int UNSIGNED NULL DEFAULT NULL,
  `updated_by` int UNSIGNED NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 116 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tshirt_raw_materials
-- ----------------------------
INSERT INTO `tshirt_raw_materials` VALUES (56, 20, 1, 3, NULL, 1, NULL, NULL);
INSERT INTO `tshirt_raw_materials` VALUES (57, 20, 2, 4, NULL, 1, NULL, NULL);
INSERT INTO `tshirt_raw_materials` VALUES (58, 20, 3, 6, NULL, 1, NULL, NULL);
INSERT INTO `tshirt_raw_materials` VALUES (59, 20, 4, 10, NULL, 1, NULL, NULL);
INSERT INTO `tshirt_raw_materials` VALUES (60, 20, 5, 10, NULL, 1, NULL, NULL);
INSERT INTO `tshirt_raw_materials` VALUES (91, 22, 1, 3, 1, NULL, NULL, NULL);
INSERT INTO `tshirt_raw_materials` VALUES (92, 22, 2, 2, 1, NULL, NULL, NULL);
INSERT INTO `tshirt_raw_materials` VALUES (93, 22, 3, 55, 1, NULL, NULL, NULL);
INSERT INTO `tshirt_raw_materials` VALUES (94, 22, 4, 23, 1, NULL, NULL, NULL);
INSERT INTO `tshirt_raw_materials` VALUES (95, 22, 5, 3434, 1, NULL, NULL, NULL);
INSERT INTO `tshirt_raw_materials` VALUES (96, 17, 1, 1, NULL, 1, NULL, NULL);
INSERT INTO `tshirt_raw_materials` VALUES (97, 17, 2, 0, NULL, 1, NULL, NULL);
INSERT INTO `tshirt_raw_materials` VALUES (98, 17, 3, 0, NULL, 1, NULL, NULL);
INSERT INTO `tshirt_raw_materials` VALUES (99, 17, 4, 0, NULL, 1, NULL, NULL);
INSERT INTO `tshirt_raw_materials` VALUES (100, 17, 5, 45, NULL, 1, NULL, NULL);
INSERT INTO `tshirt_raw_materials` VALUES (101, 18, 1, 3, NULL, 1, NULL, NULL);
INSERT INTO `tshirt_raw_materials` VALUES (102, 18, 2, 5, NULL, 1, NULL, NULL);
INSERT INTO `tshirt_raw_materials` VALUES (103, 18, 3, 56, NULL, 1, NULL, NULL);
INSERT INTO `tshirt_raw_materials` VALUES (104, 18, 4, 456, NULL, 1, NULL, NULL);
INSERT INTO `tshirt_raw_materials` VALUES (105, 18, 5, 6546, NULL, 1, NULL, NULL);
INSERT INTO `tshirt_raw_materials` VALUES (106, 19, 1, 32, NULL, 1, NULL, NULL);
INSERT INTO `tshirt_raw_materials` VALUES (107, 19, 2, 3, NULL, 1, NULL, NULL);
INSERT INTO `tshirt_raw_materials` VALUES (108, 19, 3, 324, NULL, 1, NULL, NULL);
INSERT INTO `tshirt_raw_materials` VALUES (109, 19, 4, 234, NULL, 1, NULL, NULL);
INSERT INTO `tshirt_raw_materials` VALUES (110, 19, 5, 324, NULL, 1, NULL, NULL);
INSERT INTO `tshirt_raw_materials` VALUES (111, 21, 1, 5, NULL, 1, NULL, NULL);
INSERT INTO `tshirt_raw_materials` VALUES (112, 21, 2, 2, NULL, 1, NULL, NULL);
INSERT INTO `tshirt_raw_materials` VALUES (113, 21, 3, 5, NULL, 1, NULL, NULL);
INSERT INTO `tshirt_raw_materials` VALUES (114, 21, 4, 6, NULL, 1, NULL, NULL);
INSERT INTO `tshirt_raw_materials` VALUES (115, 21, 5, 3, NULL, 1, NULL, NULL);

-- ----------------------------
-- Table structure for tshirts
-- ----------------------------
DROP TABLE IF EXISTS `tshirts`;
CREATE TABLE `tshirts`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int UNSIGNED NULL DEFAULT NULL,
  `updated_by` int UNSIGNED NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `tshirts_name_unique`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tshirts
-- ----------------------------
INSERT INTO `tshirts` VALUES (17, 'test 1', 1, 1, '2022-06-24 20:53:08', '2022-06-24 22:29:48');
INSERT INTO `tshirts` VALUES (18, 'test 2', 1, 1, '2022-06-24 21:43:14', '2022-06-24 22:30:04');
INSERT INTO `tshirts` VALUES (19, 'test 3', 1, 1, '2022-06-24 21:43:29', '2022-06-24 22:30:20');
INSERT INTO `tshirts` VALUES (20, 'tested t-shirt', 1, 1, '2022-06-24 21:45:18', '2022-06-24 22:24:57');
INSERT INTO `tshirts` VALUES (21, 'Last Test', 1, 1, '2022-06-24 22:29:08', '2022-06-24 22:30:37');
INSERT INTO `tshirts` VALUES (22, 'Red T-shirt', 1, NULL, '2022-06-24 22:29:34', '2022-06-24 22:29:34');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'Mehedi Hasan Munna', 'munna71bd@gmail.com', '2022-06-24 11:49:32', '$2y$10$oDopo4biH0QdSYnOF7iqZ.ZjdqhsETxkTZ7E2TnYtDuxuf11S.UCK', 'storage/users/1.jpg', 'ZDuBFsAe2LWi2FvotvZ22KvpS1QzW9jN381qzeh9C2nBeoBP3yyI4H533IFS', '2022-06-24 11:49:32', '2022-06-24 11:49:32');

SET FOREIGN_KEY_CHECKS = 1;
